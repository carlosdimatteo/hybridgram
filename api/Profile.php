<?php
    // hashing => http://php.net/manual/es/function.password-hash.php
    // verify => http://php.net/manual/es/function.password-verify.php
    require_once("../requieres/pgconnection.php");
    require_once("../requieres/props.php");
    $connection = getConnection();
    $queries =getQueries();
    $id=$_REQUEST["user_id"];
    try {
        if($connection) {
            // cleaaning up
            pg_query($connection, "DEALLOCATE ALL");
            // fetching user  data from database 
            $userData = pg_prepare($connection, "UserData", $queries["profilepage"]["data"]);
            $userData = pg_execute($connection, "UserData", array($id));
            $r = pg_fetch_assoc($userData);

            // fetching users posts from database
            $posts = pg_prepare($connection, "posts", $queries["profilepage"]["posts"]);
            $posts = pg_execute($connection, "posts", array($id));
            $postArray = pg_fetch_all($posts);

            // followers and following count 
            
            $followers = pg_prepare($connection, "followers", $queries["profilepage"]["followers"]);
            $followers = pg_execute($connection, "followers", array($id));
            $followers = pg_fetch_assoc($followers)["count"];
            
            $r["followers"]=$followers;

            $following = pg_prepare($connection, "following", $queries["profilepage"]["following"]);
            $following = pg_execute($connection, "following", array($id));
            $following = pg_fetch_assoc($following)["count"];
            
            $r["following"]=$following;

            //fetch number of posts 
            $postscount = pg_prepare($connection, "postscount", $queries["profilepage"]["postscount"]);
            $postscount = pg_execute($connection, "postscount", array($id));
            $postscount = pg_fetch_assoc($postscount)["count"];
            
            $r["postscount"]=$postscount;


            if($r){
                echo json_encode([
                    "status" => 200,
                    "data"=>$r,
                    "posts"=>$postArray,
                    "res" => "User Data fetched successfully",
                ]);
                }else{
                echo json_encode([
                    "status" => 400,
                    "res" => "could not fetch user data",
                ]);
                }
        
        } else{
            //if user does not exist
            echo json_encode([
                "status" => 400,
                "res" => "error connecting to database ",
            ]);
        }
    } catch (Exception $e){
        //error in database connection
        echo json_encode([
            "status" => 400,
            "res" => "Error -> " + $e->getMessage()
        ]);
    }

?>