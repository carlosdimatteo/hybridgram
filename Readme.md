# **HybridGram**
### A Mobile App where users can post, like and comment Pictures.
### Users can also _follow_ other users to view their posts 
____
## Created by:
###     [Luis Castillo](https://github.com/CastilloLuis) and  [Carlos Di Matteo](https://github.com/carlosdimatteo)

___
made with *Ionic* Framework © 2018
