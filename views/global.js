/* 
    global / helpers functions 
    @author: Luis Castillo
    @date 15-03-2018
    version: 0.2
*/
let json = {};
const $ = (id) => document.getElementById(id);

const generateJSON = (className) => {
    const inputs = document.getElementsByClassName(className);
    let data = new Object();
    let propsName, propsValue;
    for (let i = 0; i < inputs.length; i++) {
        propsName = inputs[i].id;
        propsValue = inputs[i].value;
        data["" + propsName + ""] = propsValue;
    }
    return data;
}

const validateJSON = (data) => {
    for (let params in data) {
        if (data[params].length === 0) return true;
    }
    return false;
}

const fetching = (data, method, url) => {
    switch (method) {
        case 'GET':
            {
                fetch(url, {
                    method: 'GET'
                })
                .then((res) => res.json())
                .then((data) => {
                    for (let key in data.data) {
                        json[key] = data.data[key];
                    }
                    json.posts = [];
                    for (let i in data.posts) {

                        json.posts[i] = data.posts[i];

                    }
                    console.log(json);;
                })
                .catch((err) => {

                    alert('Error while request...' + err.message);
                })
                break;
            }

        case 'POST':
            {
                let datajson = {
                    method: 'POST',
                    body: JSON.stringify(data),
                    withCredentials: true,
                    credentials: 'same-origin',
                    headers: {
                        'Content-type': 'application/x-www-form-urlencoded'
                    }
                };
                fetch(url, datajson)
                .then((res) => res.json())
                .then((data) => {

                    // for each loop for testing purposes which will save recieved user data fro the req in Session Storage to be retrieved by other views

                    console.log(json);
                    for (key in data) {
                        if (key == "res") {
                            console.log(data.res);
                        } else if (key == "status") {
                            console.log(data.status)

                        } else if (key == "username") {
                            window.localStorage.setItem("username", data.username);
                        } else if (key == "user_id") {
                            window.localStorage.setItem("user_id", data.user_id);
                        } else if (key == "data") {
                            json = data.data;
                            console.log(data.data)
                        }
                    }
                    console.log(data.res);
                    console.log("user: " + data.username + "  user_id: " + data.user_id);;
                })
                .catch((err) => {
                    console.log(err);
                })
                break;
            }
    }

}